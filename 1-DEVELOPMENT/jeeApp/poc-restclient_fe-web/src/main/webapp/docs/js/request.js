function makeRequest(confTimeout){
    $('#apiOutput').html("Waiting for response...");
    let delay = $('#apiParamDuration').val() * 1000;
    let connectionTimeout;
    let readTimeout;
    if (confTimeout){
        connectionTimeout = $('#connTimeout').val();
        readTimeout = $('#readTimeout').val();
    }
    $.ajax({
        method: "POST",
        url: "/poc-restclient_fe-web/performRequest",
        headers: {
            'Content-Type': 'application/json'
        },
        data: JSON.stringify({
            "delay": delay,
            "connectionTimeout": parseInt(connectionTimeout),
            "readTimeout": parseInt(readTimeout)
        })
    })
        .done(function (res){ $('#apiOutput').html(
            `<pre><code>${res.data}</code></pre>`
        )})
        .fail(function (err){
            $('#apiOutput').html(`<pre><code>${JSON.stringify(err.responseJSON)}</code></pre>`);
        });
}
