package com.mapfre.gaia.poc.config.restclient.security;

import com.mapfre.dgtp.gaia.commons.env.EnvironmentAttributes;
import com.mapfre.dgtp.gaiafrontend.config.security.AbstractGaiaLdapFrontendSecurity;
import com.mapfre.dgtp.gaiafrontend.core.filters.preauth.GaiaPreAuthenticationFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

 @Profile(EnvironmentAttributes.SPRING_REAL_SECURITY_PROFILE)
 @Slf4j
 @EnableWebSecurity
 public class RestclientFeSecurityReal extends AbstractGaiaLdapFrontendSecurity {

     @Autowired
     RestclientFeSecurityConfigurer restclientFeSecurityConfigurer;

     @Override
     protected void configureSecurityHttp(HttpSecurity http) throws Exception {
         restclientFeSecurityConfigurer.configure(http,
                 obtainUserInfoAuthenticationSuccessHandler());
     }

     //To activate SSO, simply include only preauthenticationFilter.
     @Bean
     public AbstractPreAuthenticatedProcessingFilter gaiaPreAuthenticationFilter() {
         GaiaPreAuthenticationFilter gaiaPreAuthenticationFilter = new GaiaPreAuthenticationFilter();
         gaiaPreAuthenticationFilter.setAuthenticationDetailsSource(preAuthenticatedGaiaGrantedAuthoritiesWebAuthenticationDetails());
         try {
             gaiaPreAuthenticationFilter.setAuthenticationManager(authenticationManager());
         } catch (Exception e) {
             log.error("There was an error in set AuthenticationManager: "+e.getMessage());
         }
         return gaiaPreAuthenticationFilter;
     }
 }