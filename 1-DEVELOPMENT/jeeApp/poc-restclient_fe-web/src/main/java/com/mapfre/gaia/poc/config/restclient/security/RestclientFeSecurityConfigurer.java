package com.mapfre.gaia.poc.config.restclient.security;

import com.mapfre.dgtp.gaia.commons.security.annotation.GaiaSecurityConfigHttp;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public  class RestclientFeSecurityConfigurer implements GaiaSecurityConfigHttp {

    public void configure(HttpSecurity http, AuthenticationSuccessHandler handler) throws Exception {
        http
            .headers()
                .frameOptions()
                .sameOrigin()
                .and()
            .csrf()
                .disable()
            .authorizeRequests()
                // Role autorization example
                .antMatchers("/**").permitAll();
    }


}