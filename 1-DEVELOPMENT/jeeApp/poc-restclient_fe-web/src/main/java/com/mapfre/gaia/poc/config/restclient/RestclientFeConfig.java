package com.mapfre.gaia.poc.config.restclient;

import com.mapfre.dgtp.gaia.commons.annotation.DemoController;
import com.mapfre.dgtp.gaia.commons.annotation.RealController;
import com.mapfre.dgtp.gaia.commons.beans.factory.support.CustomBeanNameGenerator;
import com.mapfre.dgtp.gaiafrontend.config.annotation.EnableGaiaFrontCore;
import com.mapfre.dgtp.gaiafrontend.config.security.EnableGaiaFrontendSecurity;
import com.mapfre.dgtp.gaiafrontend.core.web.filters.GaiaFilterChainConfigurer;
import com.mapfre.gaia.poc.config.restclient.security.RestclientFeSecurityConfigurer;
import com.mapfre.gaia.poc.config.restclient.security.RestclientFeSecurityDemo;
import com.mapfre.gaia.poc.config.restclient.security.RestclientFeSecurityReal;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

@Configuration
@EnableGaiaFrontCore
@EnableGaiaFrontendSecurity(
		local = RestclientFeSecurityDemo.class,
		production = RestclientFeSecurityReal.class)
@ComponentScan(
		basePackages="com.mapfre.gaia.restclient",
		nameGenerator= CustomBeanNameGenerator.class,
		excludeFilters={
            @ComponentScan.Filter(type=FilterType.ANNOTATION,value=Controller.class),
            @ComponentScan.Filter(type=FilterType.ANNOTATION,value=DemoController.class),
            @ComponentScan.Filter(type=FilterType.ANNOTATION,value=RealController.class)
            })
public class RestclientFeConfig {

	@Bean
    public static GaiaFilterChainConfigurer gaiaFilterChainConfigurer() {
        GaiaFilterChainConfigurer gFilter = new GaiaFilterChainConfigurer();
        //Definition of user data instance
        //if class GaiaFrontEndUserInfo is specified, configure it
        //gFilter.setUserInfoClass(CustomUserInfo.class);
        return gFilter;
    }

    @Bean(name = "appPlaceholder")
    public static PropertyPlaceholderConfigurer appPlaceholderConfigurer() {
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setIgnoreResourceNotFound(true);
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyPlaceholderConfigurer;
    }
	
	@Bean
	public RestclientFeSecurityConfigurer securityConfigurer() {
		return new RestclientFeSecurityConfigurer();
	}

}