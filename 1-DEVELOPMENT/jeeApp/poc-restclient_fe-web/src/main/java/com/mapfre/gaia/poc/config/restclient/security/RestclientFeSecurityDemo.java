package com.mapfre.gaia.poc.config.restclient.security;

import com.mapfre.dgtp.gaia.commons.env.EnvironmentAttributes;
import com.mapfre.dgtp.gaiafrontend.config.security.AbstractGaiaFrontendSecurity;
import com.mapfre.dgtp.gaiafrontend.core.security.ObtainUserInfoAuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;


@Profile(EnvironmentAttributes.SPRING_DEMO_SECURITY_PROFILE)
@EnableWebSecurity
public class RestclientFeSecurityDemo  extends AbstractGaiaFrontendSecurity {


    @Autowired
    RestclientFeSecurityConfigurer restclientFeSecurityConfigurer;

    @Override
    protected void configureSecurityHttp(HttpSecurity http) throws Exception {
        restclientFeSecurityConfigurer.configure(http,
                obtainUserInfoAuthenticationSuccessHandler());
    }

    @Bean
    public AuthenticationSuccessHandler obtainUserInfoAuthenticationSuccessHandler() {
        return new ObtainUserInfoAuthenticationSuccessHandler();
    }
	
}