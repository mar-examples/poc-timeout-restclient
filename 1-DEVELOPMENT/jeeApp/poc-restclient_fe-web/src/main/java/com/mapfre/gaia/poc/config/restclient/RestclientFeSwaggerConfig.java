package com.mapfre.gaia.poc.config.restclient;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class RestclientFeSwaggerConfig {
	
	@Value("${app.env.restapidoc.enabled:true}")
	private boolean enableSwagger;


	@Bean
	public Docket swaggerApi() {
		//@formatter:off
		return new Docket(DocumentationType.SWAGGER_2)
			.select()
				.apis(RequestHandlerSelectors.any())
				.paths(paths())
				.build()
			.pathMapping("/")
			// Examples of Tags customization
			.tags(new Tag("Banks", StringUtils.EMPTY))
			.tags(new Tag("Participants", StringUtils.EMPTY))
			.tags(new Tag("Email", StringUtils.EMPTY))
			//End Examples of Tag customization
			.apiInfo(apiInfo())
			.enable(enableSwagger);
		//@formatter:on
	}
	
	private ApiInfo apiInfo() {
		//@formatter:off
		return new ApiInfoBuilder().
			title("Restclient Frontend Api")
			.description("REST documentation.")
			.version("1.0")
			.build();
		//@formatter:on
	}
	
	/**
	 * All APIs except monitoring.
	 * @return
	 */
	private Predicate<String> paths() {
		// example: include all gaia apis except monitor and actuator:
		Predicate<String> apiGaia = PathSelectors.regex("^/api/(?!.*?(?:monitor|actuator)).*$");

		// example include training bankservice api:
		Predicate<String> apiBank = PathSelectors.regex("/bankservice/.*");

		return Predicates.or(apiGaia, apiBank);
	}

}