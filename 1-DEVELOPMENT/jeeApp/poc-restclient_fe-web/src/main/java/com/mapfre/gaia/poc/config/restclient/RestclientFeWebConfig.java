package com.mapfre.gaia.poc.config.restclient;

import com.mapfre.dgtp.gaia.actuator.EnableGaiaActuator;
import com.mapfre.dgtp.gaia.commons.beans.factory.support.CustomBeanNameGenerator;
import com.mapfre.dgtp.gaia.core.profiling.annotations.DemoController;
import com.mapfre.dgtp.gaia.core.profiling.annotations.RealController;
import com.mapfre.dgtp.gaiafrontend.web.servlet.annotation.EnableGaiaFrontendMvc;
import com.mapfre.dgtp.gaiafrontend.web.servlet.annotation.GaiaWebMvcConfigurerAdapter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static org.springframework.context.annotation.FilterType.ANNOTATION;

@Configuration
@EnableGaiaFrontendMvc	
@EnableWebMvc
@EnableGaiaActuator
@EnableSwagger2
@Import(RestclientFeSwaggerConfig.class)
@ComponentScan(
		basePackages="com.mapfre.gaia.restclient",
		nameGenerator= CustomBeanNameGenerator.class,
		includeFilters = { 
			@ComponentScan.Filter(type = ANNOTATION, value = Controller.class),
			@ComponentScan.Filter(type = ANNOTATION, value = DemoController.class),
			@ComponentScan.Filter(type = ANNOTATION, value = RealController.class) })
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class RestclientFeWebConfig extends GaiaWebMvcConfigurerAdapter {
}