package com.mapfre.gaia.restclient.domain;

import lombok.Data;

@Data
public class RestRequestPojo {

    private Integer delay;
    private Integer connectionTimeout;
    private Integer readTimeout;

    public RestRequestPojo(Integer delay) {
        this.delay = delay;
    }

    public RestRequestPojo(Integer delay, Integer connectionTimeout, Integer readTimeout) {
        this.delay = delay;
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
    }
}
