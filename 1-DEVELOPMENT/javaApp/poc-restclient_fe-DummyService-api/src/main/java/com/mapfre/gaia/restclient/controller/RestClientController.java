package com.mapfre.gaia.restclient.controller;

import com.mapfre.gaia.restclient.domain.RestRequestPojo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface RestClientController {
    ResponseEntity<String> performRequest(RestRequestPojo requestPojo);
}
