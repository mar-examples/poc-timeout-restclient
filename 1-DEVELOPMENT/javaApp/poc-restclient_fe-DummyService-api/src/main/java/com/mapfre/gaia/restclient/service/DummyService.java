package com.mapfre.gaia.restclient.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface DummyService {

    ResponseEntity<String> getData(Integer delay);
    ResponseEntity<String> getData(Integer delay, int connectionTimeout, int readTimeout);
}
