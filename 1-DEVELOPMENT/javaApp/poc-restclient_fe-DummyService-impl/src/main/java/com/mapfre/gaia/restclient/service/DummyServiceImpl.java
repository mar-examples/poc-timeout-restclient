package com.mapfre.gaia.restclient.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.SocketTimeoutException;

@Service
public class DummyServiceImpl implements DummyService{

    @Value("${dummy.service.endpoint}")
    private String serviceEndpoint;

    @Override
    public ResponseEntity <String> getData(Integer delay) {
        RestTemplate restTemplate = new RestTemplate();
        if (delay != null){
            return getResponseEntity(serviceEndpoint.concat("?duration=" + delay), restTemplate);
        }
        return getResponseEntity(serviceEndpoint, restTemplate);
    }

    @Override
    public ResponseEntity <String> getData(Integer delay, int connectionTimeout, int readTimeout) {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(connectionTimeout);
        requestFactory.setReadTimeout(readTimeout);

        RestTemplate restTemplate = new RestTemplate(requestFactory);
        if (delay != null){
            return getResponseEntity(serviceEndpoint.concat("?duration=" + delay), restTemplate);
        }

        return getResponseEntity(serviceEndpoint, restTemplate);
    }

    private ResponseEntity<String> getResponseEntity(String endpoint, RestTemplate restTemplate) {
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(endpoint, String.class);
            if (response.getStatusCode() != HttpStatus.OK)
                return new ResponseEntity<>(null, response.getStatusCode());
            return response;
        }
        catch (RestClientException e){
            if(e.getCause().getClass().equals(SocketTimeoutException.class))
                return new ResponseEntity<>(e.getMessage(), HttpStatus.GATEWAY_TIMEOUT);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
