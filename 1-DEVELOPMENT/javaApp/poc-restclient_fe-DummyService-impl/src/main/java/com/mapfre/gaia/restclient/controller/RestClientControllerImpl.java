package com.mapfre.gaia.restclient.controller;


import com.mapfre.gaia.restclient.domain.RestRequestPojo;
import com.mapfre.gaia.restclient.service.DummyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestClientControllerImpl implements RestClientController {

    private final DummyService dummyService;

    public RestClientControllerImpl(DummyService dummyService) {
        this.dummyService = dummyService;
    }

    @Override
    @RequestMapping(value = "/performRequest", method = RequestMethod.POST)
    public ResponseEntity<String> performRequest(@RequestBody RestRequestPojo request) {
        if (request.getConnectionTimeout() == null && request.getReadTimeout() == null){
            return dummyService.getData(request.getDelay());
        }
        return dummyService.getData(request.getDelay(), request.getConnectionTimeout(), request.getReadTimeout());
    }
}
