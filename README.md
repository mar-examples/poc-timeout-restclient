# POC-Timeout-RestClient #

Esta PoC consta de un ejemplo práctico acerca de los diferentes tipos timeouts configurables en un cliente REST dentro de una aplicación GAIA.

### Objetivo ###

Este ejemplo parte de la necesidad de limitar el tiempo de conexión/lectura al invocar un servicio REST desde una aplicación GAIA.
Para llevarlo a cabo, hay publicado un servicio _Dummy_ que responde a las peticiones pasado cierto periodo de tiempo configurable.
Esta es una aplicación GAIA FE que invoca al servicio utilizando la librería `Spring Rest Template` y en función de las 
distintas opciones de configuración de timeouts (explicadas más abajo) tendrá un comportamiento u otro.

### Entorno ###

* Aplicación GAIA 2.X de tipo FE generada mediante el plugin iFactory. Está pensada para ser desplegada en un servidor de aplicaciones Websphere 8
* Parte frontal conformado por un único fichero estático desde el cual se pueden lanzar las diferentes invocaciones al servicio estableciendo timeouts y tiempos de respuesta.
Se accede desde la raíz del `context-root` de la aplicación: `/poc-restclient_fe-web/`

### Formas de configurar los timeouts ###

Se distinguen dos formas con distinto nivel de prioridad:

1.Mediante propiedades JVM del servidor de aplicaciones (Prioridad __baja__). Incluyendo las siguientes propiedades en las opciones de la JVM del servidor:

```
-Dsun.net.client.defaultConnectTimeout=<TimeoutInMiliSec>
-Dsun.net.client.defaultReadTimeout=<TimeoutInMiliSec>
```

2.Configurando los timeouts de Rest Template programáticamente (Prioridad __alta__). En caso de realizar la siguiente configuración programática, se sobreescriben las propiedades configuradas
en el servidor de aplicaciones:
   
```
SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
requestFactory.setConnectTimeout(connectionTimeout);
requestFactory.setReadTimeout(readTimeout);

RestTemplate restTemplate = new RestTemplate(requestFactory);
...
```

### Construcción y despliegue ###

Como se indica arriba, esta aplicación está pensada para desplegarse en un servidor de aplicaciones Websphere. Por lo tanto, hemos incluido todas las dependencias, que normalmente deberían
configurarse de manera independiente a la aplicación, en el propio paquete `.ear`.

Para generarlo, basta con ejecutar:
```
mvn clean package
```
Como es habitual, podremos encontrar dicho `.ear` en `1-DEVELOPMENT/jeeApp/poc-restclient_fe-ear/target` 
